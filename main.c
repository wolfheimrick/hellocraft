#include <unistd.h>

void print_char(char c){
	write(1, &c,1);
}

void print_string(char *s) {
	while (*s != NULL) {
		print_char(*s++);
	}
}

int main() {
	print_string("hello world\n");
	return 0;
}
